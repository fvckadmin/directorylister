# Directory Lister v2.6.1 -Modified version

### 修正特性：

- 不一樣的主頁樣式
- 全面支援漢字
- 支援Readme
- CDN本地化
- any else ...

### 修正演示：

逗比云 https://softs.fun

### 安裝：

直接釋放文檔到目標檔案夾

#### 文檔架構
假設主檔案夾 `/home/wwwroot/xxx.xx`
``` bash
/home/wwwroot/xxx.xx
├─ resources
│   ├ themes
│   │ └ bootstrap
│   │    └ .....
│   │
│   ├ DirectoryLister.php
│   ├ config.php
│   └ fileTypes.php
│
├ README.html # 自設Readme #
├ index.php
│
├─ 測試檔案夾
│   ├ 測試文檔.txt
│   └ README.html # 自設Readme #
│
└ 測試文檔.txt
```
### 關於無顯示任何文檔問題：

可能系 PHP函數` scandir `被禁用。
``` bash
sed -i 's/,scandir//g' /usr/local/php/etc/php.ini
# 啟用scandir函數
/etc/init.d/php-fpm restart
# Restart PHP
```

#### Readme功能

使用这個功能之前，需要先修改` resources\themes\bootstrap\index.php `文檔的第五行：
``` bash
$md_path = explode("com", $md_path_all);
```
將` com `改成您的個人域名後綴(譬如` google.com.hk `提取` hk `寫入)

即可在` README.html `文檔中寫入自設的內容。

建議將` README.html `文檔用 UTF-8無BOM編碼 存儲！

#### 另：

修正主頁` DOUBI Soft `標題：

` \resources\DirectoryLister.php `

` \resources\themes\bootstrap\index.php `

修正二級檔案夾頁面錯亂：（在 <link href=" 后添加二級檔案夾）

` \resources\themes\bootstrap\index.php `

——————

base on Directory Lister http://www.directorylister.com/ & https://doub.io/dbrj-3/
